# Scraper

## Different concurrency patterns

Let's try this out a few different ways.

1. Super async
2. Worker pool
3. TBA

### 1. Super async

For this attempt, we will implement it like in the golang project and just fire off pages as we find them.

We will hold the state in a process and the way we will implement it will guarantee that we won't have to use a mutex but have the same results.

The way you would do something similar in golang would be to:

```go
type State struct {
  incoming chan string
  scrapedURLs map[string]bool
}

func (s *State) ShouldScrape(uri string, responseChan chan bool) {
  defer close(responseChan)
  if _, ok := s.scrapedURLs[uri]; ok {
    // We have scraped the uri
    resonseChan <- false
    return
  }

  s.scrapedURLs[uri] = true
  resonseChan <- true
}

state = State{}
answer = make chan bool
go state.ShouldScrape(answer)
shouldScrape := <- answer
```

In Elixir this is done with processes and message passing.
You can think of a mailbox as an infinitely (if you have infinite memory) buffered channel.

The process is in an infinite blocking loop waiting for messages being sent to its mailbox which it processes sequentially. This is where we get our atomic guarantee. But this is also where bottlenecks can happen so keep that in mind.

The above Go code in Elixir might look like:

```elixir
defmodule State do
  def start do
    state = %{}
    {:ok, pid} = Task.start(fn -> loop(state) end)
    pid
  end

  def loop(state) do
    receive do
      {:url, url, sender_process} ->
        {should_scrape, new_state} = check_url(url)
        send(sender_process, {:url_response, should_scrape})
        loop(new_state)
      unknown ->
        IO.puts("Unknown message to State: #{inspect unknown}")
        loop(state)
    end
  end
end

state = State.start()
send(state, {:url, "https://aaroncruz.com", self()})

{:ok, should_scrape} =
  receive do
    {:url_response, url} -> {:ok, url}
  after
    5000 ->
      {:error, "Timeout"}
  end

IO.puts("Should I scrape? #{should_scrape}")
```
