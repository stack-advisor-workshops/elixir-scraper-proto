dev:
	iex -S mix phx.server

db:
	docker run -p 5432:5432 --rm --name scrapey -v $(shell pwd)/tmp/postgres:/var/lib/postgresql/data -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=postgres postgres:11-alpine
