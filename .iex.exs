alias Scraper.Scraping.WorkerPool
alias Scraper.Scraping.WorkerPoolScraper
url = "https://www.tracylum.com/"

scrape = fn url, worker_count ->
  {:ok, _pool} = WorkerPool.start_link(worker_count)
  WorkerPoolScraper.scrape_all(url, Ecto.UUID.generate())
end
