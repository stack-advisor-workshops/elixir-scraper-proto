# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :scraper,
  ecto_repos: [Scraper.Repo]

config :scraper, :solr, url: "http://localhost/8983/scraper"

# Configures the endpoint
config :scraper, ScraperWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "PjwW7Y0K+GOB+Tr1uxluU/cXN/lDfg/hPUAh7YacRoSRyAZzpmjKSON9Qg6hAFKw",
  render_errors: [view: ScraperWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Scraper.PubSub,
  live_view: [signing_salt: "mUHq/xcw"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
