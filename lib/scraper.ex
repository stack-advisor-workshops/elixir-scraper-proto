defmodule Scraper do
  @moduledoc """
  """
  alias Scraper.Scraping.LinkParser
  alias Scraper.Sites.Page

  # -- Public API ------------------

  def scrape_page(site_id, url) do
    with {:ok, body} <- fetch_body(url),
         page <- Page.new(site_id, url, url, body),
         page <- LinkParser.parse(page) do
      {:ok, page}
    end
  end

  # -- Semi-Public API (mostly for testing)

  def fetch_body(url) do
    case HTTPoison.get(url, [], follow_redirect: true) do
      {:ok, %HTTPoison.Response{body: b}} ->
        {:ok, b}

      {:error, %HTTPoison.Error{} = error} ->
        {:error, error.reason}
    end
  end
end
