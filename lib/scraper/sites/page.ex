defmodule Scraper.Sites.Page do
  defstruct title: nil,
            site_id: nil,
            description: nil,
            content: nil,
            raw_body: nil,
            pages: [],
            root?: false,
            links: [],
            url: nil,
            host: nil,
            images: []

  @type page :: %__MODULE__{}

  @type t :: %__MODULE__{
          title: String.t() | nil,
          site_id: String.t() | nil,
          description: String.t() | nil,
          raw_body: String.t() | nil,
          content: String.t() | nil,
          root?: boolean(),
          pages: [page],
          links: [String.t()],
          url: String.t() | nil,
          host: String.t() | nil,
          images: [String.t()]
        }

  @spec new(String.t(), String.t(), String.t(), String.t()) :: __MODULE__.t()

  def new(site_id, host, url, body) do
    %__MODULE__{
      site_id: site_id,
      host: host,
      url: url
    }
    |> put_raw_body(body)
  end

  def pages(page) do
    page_list = page.pages
    page = Map.put(page, :pages, [])

    all_pages =
      Enum.reduce(page_list, [], fn page, all_pages ->
        pages(page) ++ all_pages
      end)
      |> Enum.reverse()

    [page | all_pages]
  end

  def put_raw_body(page, raw_body),
    do: Map.put(page, :raw_body, raw_body)

  def put_root_page(%__MODULE__{} = page, is_root \\ false),
    do: %__MODULE__{page | root?: is_root}

  def put_content(page, content),
    do: Map.put(page, :content, content)

  def put_links(page, links),
    do: Map.put(page, :links, links)

  def put_children(page, pages),
    do: Map.put(page, :pages, pages)
end
