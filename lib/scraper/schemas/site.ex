defmodule Scraper.Schemas.Site do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "sites" do
    timestamps()
  end

  @doc false
  def changeset(site, attrs) do
    site
    |> cast(attrs, [])
    |> validate_required([])
  end
end
