defmodule Scraper.Scraping.Worker do
  use GenServer

  def start_link do
    GenServer.start_link(__MODULE__, nil)
  end

  def init(_) do
    {:ok, nil}
  end

  def work(worker_pid, job) do
    GenServer.call(worker_pid, {:work, job}, 60_000)
  end

  def handle_call({:work, job}, _from, state) do
    res = Scraper.scrape_page(job.site_id, job.url)
    {:reply, res, state}
  end
end
