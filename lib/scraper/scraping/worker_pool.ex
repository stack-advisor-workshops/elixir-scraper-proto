defmodule Scraper.Scraping.WorkerPool do
  require Logger
  use GenServer
  alias Scraper.Scraping.Worker

  def start_link(worker_count) do
    GenServer.start_link(__MODULE__, worker_count, name: __MODULE__)
  end

  def init(worker_count) do
    workers =
      Enum.map(1..worker_count, fn _ ->
        {:ok, worker_pid} = Worker.start_link()
        worker_pid
      end)

    state = %{
      workers: workers,
      waiting: []
    }

    {:ok, state}
  end

  def checkout do
    GenServer.cast(__MODULE__, {:checkout, self()})

    receive do
      {:worker, worker_pid} ->
        Logger.debug("Checked out worker #{inspect(worker_pid)}")
        worker_pid

      other ->
        other
    after
      15_000 ->
        Logger.debug("Ok, not gonna get a worker")
    end
  end

  def checkin(worker) do
    GenServer.cast(__MODULE__, {:checkin, worker})
  end

  def handle_cast({:checkout, client_pid}, %{workers: [], waiting: waiting}) do
    waiting = waiting ++ [client_pid]
    {:noreply, %{workers: [], waiting: waiting}}
  end

  def handle_cast({:checkout, client_pid}, %{workers: [next_worker | workers], waiting: waiting}) do
    send(client_pid, {:worker, next_worker})
    {:noreply, %{workers: workers, waiting: waiting}}
  end

  def handle_cast({:checkin, worker_pid}, %{workers: [], waiting: [next_client | waiting]}) do
    send(next_client, {:worker, worker_pid})
    {:noreply, %{workers: [], waiting: waiting}}
  end

  def handle_cast({:checkin, worker_pid}, %{workers: workers, waiting: waiting}) do
    workers = workers ++ [worker_pid]
    {:noreply, %{workers: workers, waiting: waiting}}
  end
end
