defmodule Scraper.Scraping.LinkParser do
  require Logger
  alias Scraper.Scraping.Content
  alias Scraper.Sites.Page

  def parse(%Page{} = page) do
    page
    |> content()
    |> links()
    |> fix_links()
  end

  def content(page) do
    c = Content.from_raw_html(page.raw_body)

    Page.put_content(page, c)
  end

  def links(page) do
    l =
      page.raw_body
      |> Floki.find("a")
      |> Floki.attribute("href")

    Page.put_links(page, l)
  end

  # This isn't unique between protocols
  def fix_links(page) do
    uri = URI.parse(page.url)

    l =
      page.links
      |> Stream.map(&URI.parse/1)
      |> Stream.filter(&filter_link(&1, uri.host))
      |> Stream.map(&make_full_link(&1, uri))
      |> Stream.map(&URI.to_string/1)
      |> Enum.uniq()

    Page.put_links(page, l)
  end

  def filter_link(link_uri, host) do
    is_nil(link_uri.host) || link_uri.host == host
  end

  def make_full_link(%URI{host: nil, path: path} = link_uri, uri) do
    %URI{link_uri | path: normalize_path(path), scheme: uri.scheme, host: uri.host}
  end

  def make_full_link(link_uri, _uri), do: link_uri

  def normalize_path(nil), do: nil
  def normalize_path("/"), do: nil

  def normalize_path(path) do
    path = String.trim(path, "/")
    "/#{path}"
  end
end
