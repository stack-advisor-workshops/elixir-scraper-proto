defmodule Scraper.Scraping.Pages do
  require Logger

  alias Scraper.Scraping.State
  alias Scraper.Sites.Page

  def scrape_all(url, site_id) do
    state = State.start()
    task = Task.async(fn -> scrape_page(site_id, state, url, true) end)
    {:ok, page} = Task.await(task, 30_000)
    page
  end

  def maybe_scrape_page(site_id, state, url, is_root \\ false) do
    if State.scrape?(state, url) do
      Logger.debug("** Scraping #{url} **")
      scrape_page(site_id, state, url, is_root)
    else
      Logger.debug("Skipping url #{url}..")
      :skipped
    end
  end

  def scrape_page(site_id, state, url, is_root) do
    case Scraper.scrape_page(site_id, url) do
      {:ok, page} ->
        page = Page.put_root_page(page, is_root)
        page = Page.put_children(page, scrape_children(page, state))
        {:ok, page}

      {:error, err} ->
        {:error, err}
    end
  end

  def scrape_children(page, state) do
    page.links
    |> Stream.map(fn link ->
      Task.async(fn ->
        maybe_scrape_page(page.site_id, state, link)
      end)
    end)
    |> Stream.map(&Task.await(&1, 30_000))
    |> Stream.reject(&(&1 == :skipped))
    |> Stream.filter(fn
      {:ok, _page} -> true
      {:error, _err} -> false
    end)
    |> Enum.map(fn {:ok, page} -> page end)
  end
end
