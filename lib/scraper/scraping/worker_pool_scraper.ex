defmodule Scraper.Scraping.WorkerPoolScraper do
  require Logger

  alias Scraper.Scraping.{Job, State, Worker, WorkerPool}
  alias Scraper.Sites.Page

  def scrape_all(url, site_id) do
    state = State.start()
    {:ok, page} = scrape_page(site_id, state, url)
    Page.put_root_page(page, true)
  end

  def scrape_page(site_id, state, url) do
    if State.scrape?(state, url) do
      worker = WorkerPool.checkout()
      job = Job.new(site_id, url)
      Logger.debug("Scraping #{url}")

      case Worker.work(worker, job) do
        {:ok, page} ->
          WorkerPool.checkin(worker)
          children = scrape_children(page, state)
          page = Page.put_children(page, children)
          {:ok, page}

        {:error, err} ->
          WorkerPool.checkin(worker)
          {:error, err}
      end
    else
      nil
    end
  end

  def scrape_children(page, state) do
    page.links
    |> Enum.map(fn link ->
      Task.async(fn ->
        scrape_page(page.site_id, state, link)
      end)
    end)
    |> Enum.map(&Task.await(&1, :infinity))
    |> Enum.reject(&is_nil/1)
    |> Enum.filter(fn
      {:ok, _page} -> true
      {:error, _err} -> false
    end)
    |> Enum.map(fn {:ok, page} -> page end)
  end
end
