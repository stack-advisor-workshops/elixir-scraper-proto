defmodule Scraper.Scraping.Content do
  require Logger

  @default_options [
    retry_length: 250,
    min_text_length: 25,
    # remove_unlikely_candidates: true,
    remove_unlikely_candidates: false,
    weight_classes: true,
    clean_conditionally: false,
    remove_empty_nodes: true,
    min_image_width: 130,
    min_image_height: 80,
    ignore_image_format: [],
    blacklist: nil,
    whitelist: nil,
    page_url: nil
  ]

  def from_raw_html(html) do
    try do
      html
      |> Readability.Helper.normalize()
      |> Readability.ArticleBuilder.build(@default_options)
      |> Readability.readable_text()
    rescue
      CaseClauseError ->
        Logger.error("CaseClauseError in from_raw_html")
        nil
    end
  end
end
