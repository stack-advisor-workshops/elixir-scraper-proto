defmodule Scraper.Scraping.State do
  def start do
    state = %{}
    {:ok, pid} = Task.start(fn -> loop(state) end)
    pid
  end

  def scrape?(state, url) do
    send(state, {:url, url, self()})

    receive do
      {:url_response, should_scrape} ->
        should_scrape
    end
  end

  def loop(state) do
    receive do
      {:url, url, sender_process} ->
        {should_scrape, new_state} = check_url(state, url)
        send(sender_process, {:url_response, should_scrape})
        loop(new_state)

      unknown ->
        IO.puts("Unknown message to State: #{inspect(unknown)}")
        loop(state)
    end
  end

  defp check_url(state, url) do
    case Map.get(state, url) do
      nil ->
        state = Map.put(state, url, true)
        {true, state}

      _ ->
        {false, state}
    end
  end
end
