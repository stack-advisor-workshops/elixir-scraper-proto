defmodule Scraper.Scraping.Job do
  defstruct [:url, :site_id]

  def new(site_id, url),
    do: %__MODULE__{site_id: site_id, url: url}
end
